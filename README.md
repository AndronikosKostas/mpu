# MPU

Libraries that you will need :

Sketch > Include Library > Manage Libraries… & Install: 

Adafruit MPU6050 Library by Adafruit : <Adafruit_MPU6050.h>

Adafruit Unified Sensor : <Adafruit_Sensor.h>
Adafruit BusIO : <Wire.h>


Links:

Everything you need is here: 
https://lastminuteengineers.com/mpu6050-accel-gyro-arduino-tutorial/

Datasheet: 
https://invensense.tdk.com/wp-content/uploads/2015/02/MPU-6000-Datasheet1.pdf 




